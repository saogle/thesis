\documentclass[12pt]{report}
\usepackage[utf8]{inputenc}
\usepackage{geometry}
\usepackage{setspace}
\usepackage{indentfirst}
\usepackage{graphicx}
\usepackage{fancyhdr}
\usepackage{tocloft}
\usepackage{xcolor} %allows for highlighting of text
\usepackage{soul} %allows for highlighting of text
\graphicspath{ {images/} }

\renewcommand{\contentsname}{TABLE OF CONTENTS}

\setlength{\cftbeforetoctitleskip}{-0.25in}

\setlength{\cftaftertoctitleskip}{0.15in}


\renewcommand\cftchapafterpnum{\vskip10pt}

\renewcommand\cftchapaftersnum{.}

\makeatletter

\renewcommand{\cftsecpresnum}{\begin{lrbox}{\@tempboxa}}

\renewcommand{\cftsecaftersnum}{\end{lrbox}}

\makeatother

\renewcommand\thechapter{\Roman{chapter}}

\renewcommand\cftchapnumwidth{2.8em}

\renewcommand\cfttoctitlefont{\hfill}

\renewcommand\cftaftertoctitle{\hfill\null\\\null\textbf{Chapter \hfill Page}}

\renewcommand{\cftchapleader}{\cftdotfill{\cftdotsep}}

\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}

\geometry{
left=1.5in,
right=1in,
top=1in,
bottom=1in}

\begin{document}

\pagenumbering{roman}

\begin{titlepage}

	\newgeometry{top=2in}
	
	\centering
	
	\vspace*{\fill}
	{\LARGE DECREASING THE ENERGY USE IN \par WASTEWATER TREATMENT \par}
	\vspace{3cm}
	{\large By \par}
	\vspace{0.25cm}
	{\large STEPHEN OGLE \par}
	\vspace{0.25cm}
	{\large Bachelor of Science in Chemical Engineering \par}
	\vspace{0.25cm}
	{\large Oklahoma State 				University \par}
	\vspace{0.25cm}
	{\large Stillwater, OK \par}
	\vspace{0.25cm}		
	{\large 2016 \par}
	\vspace{4cm}
	{\large Submitted to the Faculty of the\\}
	{\large Graduate College of the\\}
	{\large Oklahoma State University\\}
	{\large in partial fulfillment of\\}
	{\large the requirements for\\}
	{\large the Degree of\\}
	{\large MASTER OF SCIENCE\\}
	{\large May, 2018}
	\vspace*{\fill}

\newpage
	
	\centering
	
	{\LARGE DECREASING THE ENERGY USE IN \par WASTEWATER TREATMENT \par}
	\vspace{4cm}
	{\large Thesis Approved: \par}
	\vspace{1cm}
	\rule{10cm}{0.01cm}\par
	\vspace{0.1cm}
	{\large Thesis Adviser \par}
	\vspace{1cm}
	\rule{10cm}{0.01cm}\par
	\vspace{1cm}
	\rule{10cm}{0.01cm}
	\vspace{8cm}
	\vspace*{\fill}

\newpage

\begin{flushleft}

\doublespacing

Name: STEPHEN OGLE\\
Date of Degree: MAY, 2018\\
Title of Study: DECREASING THE ENERGY USE IN WASTEWATER TREATMENT\\
Major Field: ENVIRONMENTAL ENGINEERING\\
\singlespacing
Abstract:

\end{flushleft}

\newpage

\tableofcontents

\end{titlepage}

\newpage

\pagenumbering{arabic}


\chapter{INTRODUCTION}
\doublespacing
The Food, Energy, and Water (FEW) Nexus sustainability initiative has revealed the complex interaction among the respective branches, along with the challenges faced by them to meet global demands without increased environmental degradation. The relationship between water and energy is complicated due to the necessity of water for fossil-fuel energy generation and the necessity of energy for sufficient water and wastewater treatment. To reduce the water usage and greenhouse gas (GHG) emissions related to fossil-fuel energy generation, water and wastewater treatment techniques must be optimized before communities are forced to face resource allocation  ultimatums. 

Wastewater treatment is a serious concern in regards to energy usage, with an estimated 3\% of the total electricity usage in the United States being consumed by treatment facilities \cite{mccarty}. Wastewater treatment is meant to remove organic material, excess nutrients, and trace elements from water before is discharged back into the environment, meaning stringent environment regulations are in place to monitor and ensure acceptable concentrations of wastewater contaminants are met in the effluent. Along with the aforementioned regulations, the dissolved oxygen (DO) concentration in the discharged water must meet a minimum to ensure that the discharged water does not deplete a water body of dissolved oxygen when it re-enters. DO is typically used by microorganisms during the aeration treatment phase to naturally degrade the organic material in the wastewater; this biological treatment step is characterized by the biochemical oxygen demand (BOD).

A typical wastewater treatment plant (WWTP) consists of aeration treatment, as it is a biologically-based alternative to extensive chemical additives. The aeration unit pumps large amounts of air into the tank via diffusers near the bottom. Constant aeration guarantees that more than enough DO is present for organic degradation by microorganisms. Aeration is well known as the primary energy consumer at these WWTPs, though, with annual operating costs frequently constituting more than half of the entire annual operating costs of the WWTPs. As a result, aeration treatment is the largest indirect producer of GHG emissions by WWTPs. Operators at public WWTPs are not normally concerned with aeration energy usage because municipality citizens pay for the electricity in their utility bills; thus, efficient aeration comes second to ensuring discharge concentrations in the water are meeting regulations and excessive over-aeration occurs. 

This research aims to develop a scaled, novel aeration controls scheme that simultaneously provides enough DO to the wastewater during aeration and reduces the energy usage of aeration by minimizing over-aeration. The aeration controls scheme will monitor characteristics of the wastewater in real time, use that data to calculate the necessary DO with a developed numerical model, and adjust the aeration rate using a flow controller, accordingly. The controls scheme will then be scaled up for implementation at mid-size WWTPs. While various other aeration control schemes have been validated at WWTPs around the world, this research will bridge the gap between the small and large WWTPs with uncommon, experimental aeration treatment setups where the majority of research has been focused. Because BOD is difficult to quantify quickly, the novel aeration controls scheme will utilize other important wastewater characteristics, such as DO, nitrate concentrations, ammonia concentrations, pH, and temperature, that can be ascertained from probes seemingly instantaneously.

\singlespacing
\newpage

\restoregeometry

\chapter{REVIEW OF LITERATURE}

\chapter{METHODOLOGY}

\section{\normalfont The Role of Microbiology in WWTP Aeration Treatment}

The purpose of WWTPs is to remove organics and other toxic pollutants from the influent wastewater before plant discharge. Along with several physical treatment processes, biological treatment is a primary method for reducing organic pollutant concentrations. Microorganisms maintained in activated sludge treatment processes degrade the organic pollutants by utilizing dissolved oxygen as the electron acceptor in the biochemical reaction. The necessary amount of DO for the bacteria to degrade organic constituents is dependent on the biochemical oxygen demand (BOD), a quantitative measure of the amount of organic pollutants. The DO concentrations are supplied in aeration basins via diffusers located near the bottoms of the basins that have air pumped through them. Determining the BOD for wastewater takes several days when using conventional testing methods, so it is impractical to adjust aeration rates for wastewater that will soon or has already been pumped through to the next unit once the BOD is known. This unknown factor is a primary cause for unnecessary, excessive aeration because the BOD concentration of the discharged water is stringently monitored by regulatory agencies to ensure discharge water bodies are not depleted of oxygen. 

Along with microbes that degrade organic compounds, there are bountiful communities of microbes that also perform nitrification, denitrification, and phosphorous conversion reactions that make removal of these compounds from wastewater simpler. The bacteria are aerobic lithotrophs, meaning they reduce inorganic constituents (i.e. nitrogen and phosphorous compounds) via aerobic respiration. Similarly, the bacteria that reduce organic compounds are aerobic organotrophs, with organic compounds acting as the electron donors. In contrast, anaerobic treatment (oxygen-free treatment) utilizes anaerobic organotrophs that use less-oxidizing compounds as the electron accepts, such as nitrates and sulfates. Table \hl{X}, below, highlights a few common bacteria for the reduction reactions mentioned previously.

\begin{center}
\begin{tabular}{ {l}|{l}|{l}|{l}|{l} }
	\multicolumn{5}{l}{Table \hl{X}: Commonly Utilized Bacteria in Activated Sludge Treatment}\\
	\hline
	Function & Name & Electron Donor & Electron Acceptor & Resulting Product\\
	\hline
	Organic Degredation & x & $O_2$ & Organics & $CO_2$ \\
	& x & & &\\
	& x & & &\\
	Nitrification & x & x & x & x\\
	& x & & &\\
	Denitrification & x & x & x &\\
	& x & & &\\
	Phosphate Degredation & x & x & x &\\
	& x & & &\\
\end{tabular}\\	
\end{center}


Wastewater temperatures largely dictate the rate at which the bacteria function because the biochemical reactions occurring, just like chemical reactions, are dependent on the system's energy. As a result, bacteria require more time to degrade pollutants as the temperature of the wastewater decreases. The temperature of the wastewater does not only affect the bacteria functionality, though; at higher temperatures air diffused into the system is more quickly lost to the atmosphere because the saturation concentration of the wastewater decreases. Thus, more aeration is needed during warm-weather seasons to maintain adequate microbe treatment of the wastewater. In contrast, during cold-weather seasons the saturation concentration of air in wastewater decreases. This increase in saturation concentration is hindered, though, by the decrease in microbial functionality, so excessive aeration is common so that bacteria can more easily find and utilize the oxygen. 

\chapter{FINDINGS}

\chapter{CONCLUSION}

\chapter{REFERENCES}

\chapter{APPENDICES}

\bibliographystyle{apalike}
\bibliography{thesisbibfile}

\end{document}